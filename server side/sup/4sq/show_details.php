<?php
include('config.php');
require('connect.php');


if($_GET['post_checkins']=='post_checkins')
{
	$url = 'https://api.foursquare.com/v2/checkins/add';
	$fields = array(
				'venueId'=>'4f8f1902e4b0634d37a11692',
				'venue'=>'oakwood-lounge',
				'shout'=>'Checked in at the Lounge!',
				'broadcast'=>'facebook,public',
				'll'=>'18.511130226731282,73.92976999282837',
				'oauth_token'=>$_SESSION['fetchoauth']
			);
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string,'&');
	
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST,count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 	
	curl_exec($ch);
	curl_close($ch);
	header('Location: show_details.php');
        exit;
	
}



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>AtomX</title>
    </head>
    <body>
        <h2>Badges</h2><br />
        <?php
                $badges =json_decode(file_get_contents("https://api.foursquare.com/v2/users/self/badges?oauth_token={$_SESSION['fetchoauth']}"));
                foreach($badges->response->badges as $badge):
                    if($badge->image->name!='/default_off.png')
                        echo "<img src='{$badge->image->prefix}{$badge->image->sizes[0]}{$badge->image->name}' style='margin-right:10px;float:left;' width='50' height='50' />";			
                endforeach;
        ?>
        <div style="clear:both"></div>  
        <h2>Check-Ins</h2>
        <?php
                $check_ins =json_decode(file_get_contents("https://api.foursquare.com/v2/users/self/checkins?oauth_token={$_SESSION['fetchoauth']}"));
                foreach($check_ins->response->checkins->items as $check_in):
                    echo "<b>shout</b>:".$check_in->shout.' <b>venue</b>:'.$check_in->venue->name.' <b>location</b>:'.$check_in->venue->location->address." <b>lat</b>:".$check_in->venue->location->lat." <b>lng</b>:".$check_in->venue->location->lng."<br />";
			
                endforeach;


session_destroy();
                
        ?>
        <div style="clear:both"></div>
        
    </body>
</html>