fabric.Cropzoomimage = fabric.util.createClass(fabric.Image, 
{
	type: 'cropzoomimage',
	zoomedXY: false,
	initialize: function(element, options) 
	{
		options || (options = {});
		this.callSuper('initialize', element, options);
		this.set({
			orgSrc: element.src,
			cx: 0, // clip-x
			cy: 0, // clip-y
			cw: element.width, // clip-width
			ch: element.height // clip-height
		});
	},

	zoomBy: function(x, y, z, callback) 
	{
		if (x || y) { this.zoomedXY = true; }
		this.cx += x;
		this.cy += y;

		if (z) {
			this.cw -= z;
			this.ch -= z/(this.width/this.height);
		}

		if (z && !this.zoomedXY) { 
			// Zoom to center of image initially
			this.cx = this.width / 2 - (this.cw / 2);
			this.cy = this.height / 2 - (this.ch / 2);
		}

		if (this.cw > this.width) { this.cw = this.width; }
		if (this.ch > this.height) { this.ch = this.height; }
		if (this.cw < 1) { this.cw = 1; }
		if (this.ch < 1) { this.ch = 1; }
		if (this.cx < 0) { this.cx = 0; }
		if (this.cy < 0) { this.cy = 0; }
		if (this.cx > this.width - this.cw) { this.cx = this.width - this.cw; }
		if (this.cy > this.height - this.ch) { this.cy = this.height - this.ch; }

		this.rerender(callback);
	},

	rerender: function(callback) 
	{
		var img = new Image(), obj = this;
		img.onload = function() {
			var canvas = fabric.util.createCanvasElement();
			canvas.width = obj.width;
			canvas.height = obj.height;
			canvas.getContext('2d').drawImage(this, obj.cx, obj.cy, obj.cw, obj.ch, 0, 0, obj.width, obj.height);

			img.onload = function() {
				obj.setElement(this);
				obj.applyFilters(demo.canvas.renderAll.bind(demo.canvas));
				obj.set({
					left: obj.left,
					top: obj.top,
					angle: obj.angle
				});
				obj.setCoords();
				if (callback) { callback(obj); }
			};
			img.src = canvas.toDataURL('image/png');
		};
		img.src = this.orgSrc;
	},

	toObject: function()
	{
		return fabric.util.object.extend(this.callSuper('toObject'), {
			orgSrc: this.orgSrc,
			cx: this.cx,
			cy: this.cy,
			cw: this.cw,
			ch: this.ch
		});
	}
});

fabric.Cropzoomimage.async = true;
fabric.Cropzoomimage.fromObject = function(object, callback) {
	fabric.util.loadImage(object.src, function(img) {
		fabric.Image.prototype._initFilters.call(object, object, function(filters) {
			object.filters = filters || [];
			var instance = new fabric.Cropzoomimage(img, object);
			if (callback) { callback(instance); }
		});
	}, null, object.crossOrigin);
};


var canvasModule = (function($, window) {

    var canvas;

    init = function() {
        initCanvas();
    };

    initCanvas = function() {
        canvas = new fabric.Canvas('canvas');
        var windowDimensions = commonModule.getWindowDimensions();
        canvas.setHeight(580);
        canvas.setWidth(800);
        canvas.backgroundColor="black";
        canvas.renderTop();
        canvas.setOverlayImage('img/fixed.png', canvas.renderAll.bind(canvas));

    };

    addImage = function(url) {
        fabric.Image.fromURL(url, function(img) {
            img.set({
                left: 50,
                top: 100,
            });
            img.scale(0.5);
            canvas.add(img).renderAll();
            canvas.setActiveObject(img);
        });
    };

 

   addText = function(){ //http://fabricjs.com/fabric-intro-part-2/#text
        var text = $('#input-text').val();
        var fabricText = new fabric.Text(text,
            { left: 100, top: 100, fontFamily: 'Times New Roman', stroke: '#fff',
  strokeWidth: 0});
        canvas.add(fabricText);
    };

    disableDrawingMode = function(){
        canvas.isDrawingMode = false;
    };
    
     removeObject = function(){
         canvas.remove(canvas.getActiveObject());
    };

    addDrawing = function(){ //http://fabricjs.com/fabric-intro-part-4/#free_drawing
        canvas.isDrawingMode = true;
        canvas.freeDrawingBrush.color ="#fff"
        //canvas.remove(canvas.getActiveObject());
        
    };
    
   

   
 
    return {
        canvas: canvas,
        init: init,
        addImage : addImage,
        addText : addText,
        addDrawing: addDrawing,
        removeObject: removeObject,
        disableDrawingMode : disableDrawingMode
    };

})(jQuery, window);