<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>CONVERSE COLLAGE</title>
 <link rel="stylesheet" href="css/main.css">
</head>

<body>
<div id="wrapper">
            <div id="message" style="display: none;">
            </div>
            <div id="waiting" style="display: none;">
                Please wait<br />
                <img src="img/ajax-loader.gif" title="Loader" alt="Loader" />
            </div>
            <form action="" id="demoForm" method="post">
                <fieldset>
                
    <canvas id="canvas"></canvas>

    <div id="tools">
        <button type="button" class="picker-btn" data-type="photo" id="google_drive">Add Photo</button>
        <button type="button" class="picker-btn" data-type="image" id="google_image">Add Image</button>  
        <input name="image_file" id="imageInput" type="file" id="upload_image"/>
        <button type="button" class="picker-btn" data-type="recording">Add Recording</button>
        <input type="text" placeholder="Type your text" id="input-text" />
        <button type="button" class="picker-btn" data-type="text">Add Text</button>
        <button type="button" class="picker-btn" data-type="draw">Add Drawing</button>
        <button type="button" id="delete-item" onclick="deleteItem()" value="delete">Delete</button>

    </div>
   
    
     
                
                    <p>
                        <label for="email">User ID:</label>
                        <div id = "userid">
			                  </div>
                    </p>
                    <p>
                        <label for="url">Username:</label>
                        <div id="status">
			                  </div>
                    </p>
                    
                    <p>
                        <input type="submit" name="submit" id="submit" style="float: right; clear: both; margin-right: 3px;" value="Submit" />
                    </p>
                </fieldset>
            </form>
</div>
    

    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="js/libs/purl.js"></script>
    <script src="js/libs/fabric.min.js"></script>
    <script type="text/javascript" src="js/ajaxSubmit.js"></script>

    <script src="js/common.js"></script>
    <script src="js/canvas.js"></script>
    <script src="js/picker.js"></script>
    <script src="js/main.js"></script>
    <script>

    window.onload = function(){
        $.getScript( "https://apis.google.com/js/api.js?onload=onGoogleApiLoad"); //load google apis
    }
    

    function onGoogleApiLoad() { //initialize site
        mainModule.init();
        
        $('#imageInput').change(function(e) {
        var file = e.target.files[0],
            imageType = /image.*/;
        
        if (!file.type.match(imageType))
            return;
        
        var reader = new FileReader();
        reader.onload = fileOnload;
        reader.readAsDataURL(file);
        
    });

     function fileOnload(e) {
        
	canvasModule.addImage(e.target.result);
       
    }
    
   
    }
    
    
    
    function saveUpload()
    {
            
    
    	var userid = document.getElementById("userid").innerHTML;
        var imgData = document.getElementById("canvas").toDataURL();
       
        canvasURL = canvas.toDataURL();
	var image = new Image();
	image.src = canvas.toDataURL("image/png");
	

        $('.canvas-container').replaceWith(image);
        $('#tools').hide();
        
        
        
	var ajax;
    	var xmlHttpReq = false;    
    	
    	document.getElementById('hidden_data').value = image.src;
    	var fd = new FormData(document.forms["form1"]);  
    	
    	if (window.XMLHttpRequest) {
      	  ajax = new XMLHttpRequest();
    	}

    	else if (window.ActiveXObject) {
        	ajax = new ActiveXObject("Microsoft.XMLHTTP");
    	}
   	
   	ajax.open('POST', 'upload_data.php', false);
   	
   	ajax.setRequestHeader('Content-Type', 'x-www-form-urlencoded');
   	ajax.onreadystatechange = function() {
        console.log(ajax.responseText);
    	}
    	
    	
   		ajax.send("imgData="+imgData); 
    
        
    };

    </script>
    
    <script>
  // This is called with the results from from FB.getLoginStatus().
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '598614586915510',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.1' // use version 2.1
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
     
      document.getElementById('userid').innerHTML = response.id;
      document.getElementById('status').innerHTML = response.name;
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
</fb:login-button>

</body>
</html>