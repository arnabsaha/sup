<?php
session_start();
if(!isset($_SESSION['user_id']))
header("location:index.php");

include "inc/inc_globals.php";
include "inc/inc_connect.php";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Miami ::  Home Page</title>
<link rel="stylesheet" type="text/css" href="admin.css" />

<!-- ajax auto refresh -->
<!--<script type="text/javascript">
      function AutoRefresh(){
        var xmlHttp;
        try{
          xmlHttp=new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }
        catch (e){
          try{
            xmlHttp=new ActiveXObject("Msxml2.XMLHTTP"); // Internet Explorer
          }
          catch (e){
            try{
              xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e){
              alert("No AJAX");
              return false;
            }
          }
        }

        xmlHttp.onreadystatechange=function(){
          if(xmlHttp.readyState==4){
            document.getElementById('AutoUpdte').innerHTML=xmlHttp.responseText;
			//alert("3 sec");
            //setTimeout('AutoRefresh()',3000); // JavaScript function calls AutoRefresh() every 3 seconds
          }
        }
        xmlHttp.open("GET","home.php",true);
        xmlHttp.send(null);
      }

      AutoRefresh();
    </script>-->
</script>
<!--<script type="text/javaScript">
    function timedRefresh(timeoutPeriod) {
	 setTimeout("window.location.reload(true);",timeoutPeriod);
    }
</script>
 onload="timedRefresh(10000);"
-->
<style>
#up1 a:hover{color:#900}
</style>
<!-- CSS styles for standard search box -->
<style type="text/css">
	#tfheader{
		background-color:#c3dfef;
	}
	#tfnewsearch{
		float:right;
		padding:20px;
	}
	.tftextinput{
		margin: 0;
		padding: 5px 15px;
		font-family: Arial, Helvetica, sans-serif;
		font-size:14px;
		border:1px solid #0076a3; border-right:0px;
		border-top-left-radius: 5px 5px;
		border-bottom-left-radius: 5px 5px;
	}
	.tfbutton {
		margin: 0;
		padding: 5px 15px;
		font-family: Arial, Helvetica, sans-serif;
		font-size:14px;
		outline: none;
		cursor: pointer;
		text-align: center;
		text-decoration: none;
		color: #ffffff;
		border: solid 1px #0076a3; border-right:0px;
		background: #0095cd;
		background: -webkit-gradient(linear, left top, left bottom, from(#00adee), to(#0078a5));
		background: -moz-linear-gradient(top,  #00adee,  #0078a5);
		border-top-right-radius: 5px 5px;
		border-bottom-right-radius: 5px 5px;
	}
	.tfbutton:hover {
		text-decoration: none;
		background: #007ead;
		background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
		background: -moz-linear-gradient(top,  #0095cc,  #00678e);
	}
	/* Fixes submit button height problem in Firefox */
	.tfbutton::-moz-focus-inner {
	  border: 0;
	}
	.tfclear{
		clear:both;
	}
	
</style>

</head>

<body id="AutoUpdte">
<div class="wrapper_out">
	<div class="banner">
    <div class="logo"> </div>
        <div class="linking">
            
            <p class="last_login"><strong>Welcome </strong><?php echo $_SESSION['user_name']; ?> | <a href="logOut.php" style="color:#FFF" >Logout</a></p>
            <br><br>
             <p class="last_login"> <strong>Back to </strong><a href="option.php" style="color:#FFF" >Option Page</a></p>
          <div class="clear"></div>
            <div class="clear"></div>
           
        	<div class="clear"></div>
        </div><!--linking end-->
        <div class="clear"></div>
        <!-- HTML for SEARCH BAR -->
        
    </div><!--banner end-->
    <div id="tfheader">
            <form id="tfnewsearch" method="get" action="home.php">
                    <input type="text" class="tftextinput" name="q" size="21" maxlength="120"><input type="submit" value="search" class="tfbutton">
            </form>
        	<div class="tfclear"></div>
        </div>
    <div class="login_page">
    	
    	<h1 class="fleft">Facebook Users </h1>
        <!--<span class="fright"><a href="#" id="plus">Back</a></span>-->
      <!--  <span class="fright"><a href="#" id="back">Back</a></span>-->
        <div class="clear"></div>
        <h1></h1>
        <div >
        <?php
		if(isset($_GET['q']))
			{
				$search="(fb.uid like '%".$_GET['q']."%') OR (fb.user_name like '%".$_GET['q']."%')";
			}
			else
			{
				$search="1";
			}
            $query="SELECT fb.uid,fb.user_name,count(trac.uid) As trac_count FROM facebook fb
            LEFT OUTER JOIN track trac ON fb.uid=trac.uid  
            WHERE ".$search." 
            GROUP BY uid
            ORDER BY uid ASC";
            $result=mysql_query($query);
			$num=mysql_num_rows($result);
			if($num>0)
			{
		?>
        <table cellpadding="0" cellspacing="1" class="info_table">
            <tr>
                <th>Uid</th>
                <th>Name</th>
                <th>Count</th>
                <th></th>
            </tr>
			<?php
			
			
				$i=1;
				while($row=mysql_fetch_assoc($result))	
				{
					echo "
					<tr>
					<input type='hidden' id='".$i."' name='".$i."' value='".$row["uid"]."' />
						<td>".$row['uid']."</td>
						<td>".$row['user_name']."</td>
						<td title='Click to check Timestamp'><a href='timestamp.php?uid=".$row['uid']."' style='color:#F00'><b>".$row['trac_count']."</b></a></td>
						<td title='Go to Tools Page'><a href='tools.php?uid=".$row['uid']."&flag=fb' style='color:#300;text-decoration:none'><b>Tools</b></a></td>
				   </tr>";
					 $i++;          
				}
			
           ?>
         </table>
         <?php
		 }
		else
		{
			echo"<table cellpadding='0' cellspacing='1' class='info_table'>
            <tr><td>No match Found</td></tr></table>";	
		}
		 ?>
        </div>
        
        <h1 class="fleft">SUP Users </h1>
        <!--<span class="fright"><a href="#" id="plus">Back</a></span>-->
      <!--  <span class="fright"><a href="#" id="back">Back</a></span>-->
        <div class="clear"></div>
        <h1></h1>
        <div >
        <?php
		if(isset($_GET['q']))
			{
				$search1="(sup.uid like '%".$_GET['q']."%') OR (sup.user_name like '%".$_GET['q']."%')";
			}
			else
			{
				$search1="1";
			}
            $query1="SELECT sup.uid,sup.user_name,count(trac.uid) As trac_count FROM sup sup
            LEFT OUTER JOIN track trac ON sup.uid=trac.uid  
            WHERE ".$search1."
            GROUP BY uid
            ORDER BY uid ASC";
            $result1=mysql_query($query1);
			$num1=mysql_num_rows($result1);
		if($num1>0)
		{
		?>
        <table cellpadding="0" cellspacing="1" class="info_table">
            <tr>
                <th>Uid</th>
                <th>Name</th>
                <th>Count</th>
                <th></th>
            </tr>
    		<?php
			
			$i=1;
            while($row1=mysql_fetch_assoc($result1))	
            {
				$s="a".$i."";
                echo "
                <tr><input type='hidden' id='".$s."' name='".$s."' value='".$row1["uid"]."' />
                    <td>".$row1["uid"]."</td>
					<td>".$row1["user_name"]."</td>
					<td title='Click to check Timestamp'><a href='timestamp.php?uid=".$row1["uid"]."' style='color:#F00'><b>".$row1["trac_count"]."</b></a></td>
					";
					?>
					
                    <?php echo"
					<td title='Go to Tools Page'><a href='tools.php?uid=".$row1['uid']."&flag=sup' style='color:#300;text-decoration:none'><b>Tools</b></a></td>
               </tr>";
               $i++;        
            }
           ?>
         </table>
          <?php
		}
		else
		{
			echo"<table cellpadding='0' cellspacing='1' class='info_table'>
            <tr><td>No match Found</td></tr></table>";	
		}
		 ?>
        </div>
        
    </div><!--login_page end-->
</div><!--wrapper_out end-->
<div class="footer_out">
	<div class="footer_in">
    	Copyright ©   
    </div><!--footer_in end-->
</div><!--footer_out end-->
</body>
</html>
</html>








	<script type="text/javascript">
    function myfun(str)
    {
	var id1=document.getElementById(str).value;
	
    var r=confirm("Do You Really Want to Delete?");
    if (r==true)
      {
     window.location = "process.php?uid="+id1+"";
      }
    else
      {
      window.location = "home.php";
      }
    }
	
	
	function myUpgradeDelete(str1)
    {
	var id=document.getElementById(str1).value;
	
    var r=confirm("Do You Really Want to Delete?");
    if (r==true)
      {
     window.location ="process_sup.php?uid="+id+"";
      }
    else
      {
      window.location = "home.php";
      }
    }
    </script>


