// JavaScript Document

x=screen.width;
	
	if(x==1024)
	{
		x=217;
	}
	else if(x==1152)
	{
		x=280;
	}
	else if(x==800)
	{
		x=210;
	}


// USER
function update_user(user_id)
{
	window.location="updateUser.php?ACTION=update&user_id="+user_id;
}
function delete_user(user_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processUser.php?ACTION=delete&user_id="+user_id;
	}
}

// CATEGORY
function update_customer(custome_id)
{
	window.location="updateCustomer.php?ACTION=update&custome_id="+custome_id;
}
function delete_customer(custome_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processCustomer.php?ACTION=delete&custome_id="+custome_id;
	}
}


function update_aboutUs(page_id)
{
	window.location="updateInfoAbtUs.php?ACTION=update&page_id="+page_id;
}

function delete_member(member_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processMember.php?ACTION=delete&member_id="+member_id;
	}
}

function update_product_offer(product_offer_id)
{
	window.location="updateProductOffer.php?ACTION=update&product_offer_id="+product_offer_id;
}
function delete_product_offer(product_offer_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processProductOffer.php?ACTION=delete&product_offer_id="+product_offer_id;
	}
}
function delete_product_offer_amount(product_offer_amount_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processProductOffer.php?ACTION=deleteAmount&product_offer_amount_id="+product_offer_amount_id;
	}
}

function update_category(category_id)
{
	window.location="updateCategory.php?ACTION=update&category_id="+category_id;
}
function delete_category(category_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processCategory.php?ACTION=delete&category_id="+category_id;
	}
}
function update_category_banner(category_id)
{
	window.location='updateCategoryBanner.php?category_id='+category_id;
}

// BRAND
function update_brand(brand_id)
{
	window.location="updateBrand.php?brand_id="+brand_id;
	
}
function delete_brand(brand_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processBrand.php?ACTION=delete&brand_id="+brand_id;
	}
}

// COLOR CODE
function update_color_code(color_code_id)
{
	window.location="updateColorCode.php?color_code_id="+color_code_id;
}
function delete_color_code(color_code_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processColorCode.php?ACTION=delete&color_code_id="+color_code_id;
	}
}

// PRODUCT
function update_product(product_id)
{
	window.location="updateProduct.php?product_id="+product_id;
}
function delete_product(product_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processProduct.php?ACTION=delete&product_id="+product_id;
	}
}

// PRODUCT SNAP
function update_product_snap(product_snap_id)
{
	window.location="updateProductSnap.php?product_snap_id="+product_snap_id;
}
function delete_product_snap(product_snap_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processProductSnap.php?ACTION=deleteSnap&product_snap_id="+product_snap_id;
	}
}


// PRODUCT RANGE
function update_product_range(product_range_id,product_name)
{
	window.location="updateProductRange.php?product_range_id="+product_range_id+"&product_name="+product_name;
}
function delete_product_range(product_range_id)
{
	if(confirm("Are U really want to delete"))
	{
		window.location="processProductRange.php?ACTION=delete&product_range_id="+product_range_id;
	}
}


function update_product_prising(product_pricing_details_id)
{
	if(confirm("Are U really want to deactivate old setting and activate new"))
	{
		window.location="updateProductPrising.php?product_pricing_details_id="+product_pricing_details_id+"";
	}
}
