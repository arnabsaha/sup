/**
 * Copyright 2010-present Facebook.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sup.login;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;






import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.*;
import com.sup.login.R;
import com.facebook.widget.UserSettingsFragment;


public class LoginUsingLoginFragmentActivity extends FragmentActivity {
    private UserSettingsFragment userSettingsFragment;
    private UserDetailsDataSource datasource;
    
  
    
 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_fragment_activity);
        
        final TextView uid = (TextView) findViewById( R.id.uid );
        final TextView fname = (TextView) findViewById( R.id.fname );
        final TextView email = (TextView) findViewById( R.id.email );
        final TextView logoutMessage = (TextView) findViewById( R.id.logoutMessage );
        final Button register = (Button) findViewById(R.id.register);
       
       
        final Button fblogin = (Button) findViewById(R.id.com_facebook_usersettingsfragment_login_button);
        
        final Context context = this;
        
        Intent intent = this.getIntent();
		final String uidIntent = intent.getStringExtra("uid");
		Log.d("Passed UID is: ", uidIntent);
		
		uid.setText("uid: "+uidIntent);
		
		datasource = new UserDetailsDataSource(this);
	    datasource.open();
	    
	  
        FragmentManager fragmentManager = getSupportFragmentManager();
        userSettingsFragment = (UserSettingsFragment) fragmentManager.findFragmentById(R.id.login_fragment);
        
        if(isNetworkAvailable())
        {
        
        userSettingsFragment.setSessionStatusCallback(new Session.StatusCallback() {
            @Override
            public void call(final Session session, SessionState state, Exception exception) {
            	
                Log.d("LoginUsingLoginFragmentActivity", String.format("New session state: %s", state.toString()));
                
                
              
                if(state.isOpened())
                {
                	fname.setEnabled(false);
                    email.setEnabled(false);
                    register.setEnabled(false);
                    logoutMessage.setText("Please logout from Facebook to Register via SUP");
                    
                 
                    
                }
                
               
                	
            
                
                if (session.isOpened()) {
                    
                	/* List<String> permissions = new ArrayList<String>();
                     permissions.add("publish_stream");
                     
                     Session.getActiveSession().requestNewPublishPermissions(new Session.NewPermissionsRequest(LoginUsingLoginFragmentActivity.this, permissions));*/
                	 
                    final String oauth = session.getAccessToken();
                                      
                	Log.d("Access Token",session.getAccessToken());
                
                	
                   
                    // make request to the /me API
                    Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

                      
					@Override
					public void onCompleted(GraphUser user, Response response) {
					
						// TODO Auto-generated method stub
						if (user != null) {
	                          
	                          Log.d("FBUsername ",user.getName());
	                          
	                          if(session.getPermissions().contains("publish_stream"))
	                          {
	                             sendFBPostRequest(uidIntent,user.getId(),oauth,user.getName());
	                          }
					       }
					}
                    });
                  }
                else
                	if(session.isClosed())
                {
                	fname.setEnabled(true);
                    email.setEnabled(true);
                    register.setEnabled(true);
                    logoutMessage.setText("");
                    
                    Intent intent = new Intent(LoginUsingLoginFragmentActivity.this,DefaultNfcReaderActivity.class);
            		LoginUsingLoginFragmentActivity.this.startActivity(intent);
            		
                }
                	
            }
            
        }); 
        
        
     
        }
        else
        {
              fblogin.setEnabled(false);
              //fbname.setText("No Internet Access. Please register via SUP");
              Toast.makeText(getApplicationContext(), "No Internet Access. Please register via SUP", Toast.LENGTH_LONG).show();
				
        }
        
        register.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
			// TODO Auto-generated method stub
				if(fname.getText().toString().matches(""))
				{
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			 
						// set title
						//alertDialogBuilder.setTitle("Your Title");
			 
						// set dialog message
						alertDialogBuilder
							.setMessage("User Name Cannot be empty!")
							.setCancelable(false)
							
							.setNeutralButton("Okay",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, just close
									// the dialog box and do nothing
									dialog.cancel();
								}
							});
			 
							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();
			 
							// show it
							alertDialog.show();
					
				}
				else
				
				if(email.getText().toString().matches(""))
				{
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
			 
						// set title
						//alertDialogBuilder.setTitle("Your Title");
			 
						// set dialog message
						alertDialogBuilder
							.setMessage("Email Cannot be empty!")
							.setCancelable(false)
							
							.setNeutralButton("Okay",new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,int id) {
									// if this button is clicked, just close
									// the dialog box and do nothing
									dialog.cancel();
								}
							});
			 
							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder.create();
			 
							// show it
							alertDialog.show();
					
				}
				
				
				else
								
				{
				   if(isNetworkAvailable())
				   {
					
								
					            sendSQLiteDBtoServer();
								sendPostRequest(uidIntent,fname.getText().toString(),email.getText().toString());
								
								
						
				   }
				   else
				   {
					//Network not available - Storing Locally
					   
					   datasource.createComment(uidIntent,fname.getText().toString(),email.getText().toString());
					   Toast.makeText(getApplicationContext(), "Thanks for registering via SUP", Toast.LENGTH_LONG).show();
						
					   Intent intent = new Intent(LoginUsingLoginFragmentActivity.this,DefaultNfcReaderActivity.class);
					   LoginUsingLoginFragmentActivity.this.startActivity(intent);
						
						
				   }	
				     
				     
				}
			}
	     	  	});
	    
	    
	  

	      
    }
    

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        userSettingsFragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        
        
           
          
    }
    
   

    
   /* 
    public static void requestPublishPermissions(Activity activity, Session session, List<String> permissions,
    	    int requestCode) {
    	    if (session != null) {
    	        Session.NewPermissionsRequest reauthRequest = new Session.NewPermissionsRequest(activity, permissions)
    	        .setRequestCode(requestCode);
    	        session.requestNewPublishPermissions(reauthRequest);
    	    }
    	}

    	public static void requestReadPermissions(Activity activity, Session session, List<String> permissions,
    	    int requestCode) {
    	    if (session != null) {
    	        Session.NewPermissionsRequest reauthRequest = new Session.NewPermissionsRequest(activity, permissions)
    	        .setRequestCode(requestCode);
    	        session.requestNewReadPermissions(reauthRequest);
    	    }
    	} */
    
    
    public boolean isNetworkAvailable() {
	    ConnectivityManager cm = (ConnectivityManager) 
	      getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
	    // if no network is available networkInfo will be null
	    // otherwise check if we are connected
	    if (networkInfo != null && networkInfo.isConnected()) {
	        return true;
	    }
	    return false;
	} 
    
  //Send FB cred Post Request
    private void sendFBPostRequest(String uid, String id, String oauth, String name) {
		class SendPostReqAsyncTask extends AsyncTask<String, Void, String>{
			
			private ProgressDialog pDialog = new ProgressDialog(LoginUsingLoginFragmentActivity.this);

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

				pDialog.setMessage("Loading Please wait......");
				pDialog.show();

			}

		@Override
		protected String doInBackground(String... params) {
		  String paramUID = params[0];
	      String paramID = params[1];
		  String paramOAUTH = params[2];
		  String paramName = params[3];
		  
		  System.out.println("*** doInBackground ** paramUID " + paramUID + " paramID :" + paramID + " paramOAUTH :" + paramOAUTH + " paramName :" + paramName );
		  HttpClient httpClient = new DefaultHttpClient();
		  
		  // In a POST request, we don't pass the values in the URL.
		  //Therefore we use only the web page URL as the parameter of the HttpPost argument
		  HttpPost httpPost = new HttpPost("http://www.startupmedia.net/sup/miami_android/addfbuser.php");
		  // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
		  //uniquely separate by the other end.
		  //To achieve that we use BasicNameValuePair	
		  //Things we need to pass with the POST request
		  BasicNameValuePair uidBasicNameValuePair = new BasicNameValuePair("paramUID", paramUID);
		  BasicNameValuePair idBasicNameValuePair = new BasicNameValuePair("paramID", paramID);
		  BasicNameValuePair oauthBasicNameValuePAir = new BasicNameValuePair("paramOAUTH", paramOAUTH);
		  BasicNameValuePair nameBasicNameValuePAir = new BasicNameValuePair("paramName", paramName);
		  
		
		  // We add the content that we want to pass with the POST request to as name-value pairs
		  //Now we put those sending details to an ArrayList with type safe of NameValuePair
		  List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
		  nameValuePairList.add(uidBasicNameValuePair);
		  nameValuePairList.add(idBasicNameValuePair);
		  nameValuePairList.add(oauthBasicNameValuePAir);
		  nameValuePairList.add(nameBasicNameValuePAir);
		 
		try {
		  // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs. 
		  //This is typically useful while sending an HTTP POST request. 
		  UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
		  // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
		  httpPost.setEntity(urlEncodedFormEntity);
		  try {
		    // HttpResponse is an interface just like HttpPost.
		    //Therefore we can't initialize them
		    HttpResponse httpResponse = httpClient.execute(httpPost);
		    // According to the JAVA API, InputStream constructor do nothing. 
		    //So we can't initialize InputStream although it is not an interface
		    InputStream inputStream = httpResponse.getEntity().getContent();
		    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		    StringBuilder stringBuilder = new StringBuilder();
		    String bufferedStrChunk = null;
		    while((bufferedStrChunk = bufferedReader.readLine()) != null){
		    stringBuilder.append(bufferedStrChunk);
	    	}
		   return stringBuilder.toString();
		    } catch (ClientProtocolException cpe) {
		        System.out.println("First Exception caz of HttpResponese :" + cpe);
		         cpe.printStackTrace();
		     } catch (IOException ioe) {
		     System.out.println("Second Exception caz of HttpResponse :" + ioe);
		     ioe.printStackTrace();
		    }
		} catch (UnsupportedEncodingException uee) {
		System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
		uee.printStackTrace();
		}
		return null;
		}
		@Override
		protected void onPostExecute(String result) {
		super.onPostExecute(result);
		 if (pDialog.isShowing()) {

				pDialog.dismiss();
				final TextView fname = (TextView) findViewById( R.id.fname );
				final TextView email = (TextView) findViewById( R.id.email );
				final Button register = (Button) findViewById(R.id.register);
				fname.setEnabled(false);
				email.setEnabled(false);
				register.setEnabled(false);
				
				
			}
	    	 
		if(result.equals("working")){
			
	   
		Toast.makeText(getApplicationContext(), "Thanks for registering via Facebook", Toast.LENGTH_LONG).show();
		
		
		
		}else{
		Toast.makeText(getApplicationContext(), "User Already Registered", Toast.LENGTH_LONG).show();
		}
		}	
		}
		SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
		sendPostReqAsyncTask.execute(uid, id, oauth, name);	
		}
	
	private void sendSQLiteDBtoServer()
	{
		Log.d("Status","InSQLiteDBtoServer");
		List<UserDetails> values;
		values = datasource.getAllComments();
		for(int i=0;i<values.size();i++)
		{
		//System.out.print(i);
		//String toPost = ;
		System.out.print("UID: "+values.get(i).getId()+ " Name: "+values.get(i).getName()+" Email: "+values.get(i).getEmail());
		sendPostRequest(String.valueOf(values.get(i).getId()),values.get(i).getName(),values.get(i).getEmail());
		datasource.deleteComment(String.valueOf(values.get(i).getId()));
		}
		
		
	}
	
	
    private void sendPostRequest(String uid, String name, String email) {
		class SendPostReqAsyncTask extends AsyncTask<String, Void, String>{
			
			private ProgressDialog pDialog = new ProgressDialog(LoginUsingLoginFragmentActivity.this);

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

				pDialog.setMessage("Loading Please wait......");
				pDialog.show();

			}

		@Override
		protected String doInBackground(String... params) {
		  String paramUID = params[0];
	      String paramName = params[1];
		  String paramEmail = params[2];
		  System.out.println("*** doInBackground ** paramUID " + paramUID + " paramName :" + paramName + " paramEmail :" + paramEmail);
		  HttpClient httpClient = new DefaultHttpClient();
		  
		  // In a POST request, we don't pass the values in the URL.
		  //Therefore we use only the web page URL as the parameter of the HttpPost argument
		  HttpPost httpPost = new HttpPost("http://www.startupmedia.net/sup/miami_android/adduser.php");
		  // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
		  //uniquely separate by the other end.
		  //To achieve that we use BasicNameValuePair	
		  //Things we need to pass with the POST request
		  BasicNameValuePair uidBasicNameValuePair = new BasicNameValuePair("paramUID", paramUID);
		  BasicNameValuePair nameBasicNameValuePAir = new BasicNameValuePair("paramName", paramName);
		  BasicNameValuePair emailBasicNameValuePAir = new BasicNameValuePair("paramEmail", paramEmail);
		
		  // We add the content that we want to pass with the POST request to as name-value pairs
		  //Now we put those sending details to an ArrayList with type safe of NameValuePair
		  List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
		  nameValuePairList.add(uidBasicNameValuePair);
		  nameValuePairList.add(nameBasicNameValuePAir);
		  nameValuePairList.add(emailBasicNameValuePAir);
		try {
		  // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs. 
		  //This is typically useful while sending an HTTP POST request. 
		  UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
		  // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
		  httpPost.setEntity(urlEncodedFormEntity);
		  try {
		    // HttpResponse is an interface just like HttpPost.
		    //Therefore we can't initialize them
		    HttpResponse httpResponse = httpClient.execute(httpPost);
		    // According to the JAVA API, InputStream constructor do nothing. 
		    //So we can't initialize InputStream although it is not an interface
		    InputStream inputStream = httpResponse.getEntity().getContent();
		    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		    StringBuilder stringBuilder = new StringBuilder();
		    String bufferedStrChunk = null;
		    while((bufferedStrChunk = bufferedReader.readLine()) != null){
		    stringBuilder.append(bufferedStrChunk);
	    	}
		   return stringBuilder.toString();
		    } catch (ClientProtocolException cpe) {
		        System.out.println("First Exception caz of HttpResponese :" + cpe);
		         cpe.printStackTrace();
		     } catch (IOException ioe) {
		     System.out.println("Second Exception caz of HttpResponse :" + ioe);
		     ioe.printStackTrace();
		    }
		} catch (UnsupportedEncodingException uee) {
		System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
		uee.printStackTrace();
		}
		return null;
		}
		@Override
		protected void onPostExecute(String result) {
		super.onPostExecute(result);
		 if (pDialog.isShowing()) {

				pDialog.dismiss();
				final TextView fname = (TextView) findViewById( R.id.fname );
				final TextView email = (TextView) findViewById( R.id.email );
				final Button register = (Button) findViewById(R.id.register);
				fname.setEnabled(false);
				email.setEnabled(false);
				register.setEnabled(false);
				
				
			}
	    	 
		if(result.equals("working")){
			
			System.out.println(getMethodName(0));
			
		Toast.makeText(getApplicationContext(), "Thanks for registering via SUP", Toast.LENGTH_LONG).show();
		
		Intent intent = new Intent(LoginUsingLoginFragmentActivity.this,DefaultNfcReaderActivity.class);
		LoginUsingLoginFragmentActivity.this.startActivity(intent);
		
		}else{
		Toast.makeText(getApplicationContext(), "User Already Registered", Toast.LENGTH_LONG).show();
		}
		}	
		}
		SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
		sendPostReqAsyncTask.execute(uid, name, email);	
		}
    
    public static String getMethodName(final int depth) {
    	  final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
    	  return ste[1 + depth].getMethodName();
    	}
	
}



