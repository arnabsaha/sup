package com.sup.login;

public class UserDetails {
  private String uid;
  private String fname;
  private String email;

  public String getId() {
    return uid;
  }

  public void setId(String uid) {
    this.uid = uid;
  }

  public String getName() {
    return fname;
  }

  public void setName(String fname) {
    this.fname = fname;
  }
  
  public String getEmail() {
	    return email;
	  }
  
  public void setEmail(String email) {
	    this.email = email;
	  }

  // Will be used by the ArrayAdapter in the ListView
  @Override
  public String toString() {
	
	String val=String.valueOf(uid)+" "+fname+" "+email;
    return val;
    
  }
} 