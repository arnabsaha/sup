package com.sup.login;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

  public static final String SUP_REGISTER = "supmiamiregister";
  public static final String COLUMN_ID = "uid";
  public static final String COLUMN_NAME = "fname";
  public static final String COLUMN_EMAIL = "email";

  private static final String DATABASE_NAME = "supmiamiregister.db";
  private static final int DATABASE_VERSION = 1;

  // Database creation sql statement
  private static final String DATABASE_CREATE = "create table "
      + SUP_REGISTER + "(" + COLUMN_ID
      + " text primary key, " + COLUMN_NAME
      + " text not null, " + COLUMN_EMAIL + " text not null);";

  public MySQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    Log.w(MySQLiteHelper.class.getName(),
        "Upgrading database from version " + oldVersion + " to "
            + newVersion + ", which will destroy all old data");
    db.execSQL("DROP TABLE IF EXISTS " + SUP_REGISTER);
    onCreate(db);
  }

} 