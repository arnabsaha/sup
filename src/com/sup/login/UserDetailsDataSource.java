package com.sup.login;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

public class UserDetailsDataSource {

  // Database fields
  private SQLiteDatabase database;
  private MySQLiteHelper dbHelper;
  private String[] allColumns = { MySQLiteHelper.COLUMN_ID,
      MySQLiteHelper.COLUMN_NAME, MySQLiteHelper.COLUMN_EMAIL };

  public UserDetailsDataSource(Context context) {
    dbHelper = new MySQLiteHelper(context);
  }

  public void open() throws SQLException {
    database = dbHelper.getWritableDatabase();
  }

  public void close() {
    dbHelper.close();
  }

  public void createComment(String uid, String fname, String email) {
	  
	
    ContentValues values = new ContentValues();
    values.put(MySQLiteHelper.COLUMN_ID, uid);
    values.put(MySQLiteHelper.COLUMN_NAME, fname);
    values.put(MySQLiteHelper.COLUMN_EMAIL, email);
    
    try
    {
    long insertId = database.insert(MySQLiteHelper.SUP_REGISTER, null,
        values);
    Cursor cursor = database.query(MySQLiteHelper.SUP_REGISTER,
        allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
        null, null, null);

    if( cursor != null)
    {
    cursor.moveToFirst();
    cursorToComment(cursor);
    cursor.close();
    }
    
  
   
    //UserDetails newComment = 
   
   
   
    }
    
    catch (SQLiteConstraintException e)
    {
    	e.printStackTrace();
    }
  
  }

  public void deleteComment(String uid) {
    //long id = comment.getId();
    System.out.println("User deleted with id: " + uid);
    database.delete(MySQLiteHelper.SUP_REGISTER, MySQLiteHelper.COLUMN_ID
        + " = " + uid, null);
  }
  


  public List<UserDetails> getAllComments() {
    List<UserDetails> comments = new ArrayList<UserDetails>();

    Cursor cursor = database.query(MySQLiteHelper.SUP_REGISTER,
        allColumns, null, null, null, null, null);
  
    
   
    cursor.moveToFirst();
    
    
    while (!cursor.isAfterLast()) {
    	UserDetails comment = cursorToGet(cursor);
      comments.add(comment);
      cursor.moveToNext();
    }
    // Make sure to close the cursor
    cursor.close();
    return comments;
  }

  private UserDetails cursorToComment(Cursor cursor) {
	UserDetails comment = new UserDetails();
	if (cursor.moveToFirst())
	{
    comment.setId(cursor.getString(0));
    comment.setName(cursor.getString(1));
    comment.setEmail(cursor.getString(2));
	}
    return comment;
  }


private UserDetails cursorToGet(Cursor cursor) {
	UserDetails comment = new UserDetails();
	
   comment.setId(cursor.getString(0));
   comment.setName(cursor.getString(1));
   comment.setEmail(cursor.getString(2));
	
   return comment;
 }
} 
